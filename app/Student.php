<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Student extends Model
{
    //
    protected $table = 'students';

    // protected $connection = 'mongodb';
    // protected $collection = 'students';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'address'
    ];
}
